# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Basic Exports
#source ~/.config/zsh/.zprofile
export PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH="/home/arctic/.oh-my-zsh"
export HOSTNAME=$HOST
export BROWSER="chromium"
export MANPAGER='nvim +Man!'
export EDITOR='nvim'
export LANG=en_AU.UTF-8


if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='vim'
 else
   export EDITOR='nvim'
 fi

autoload -Uz compinit && compinit

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

source $ZSH/oh-my-zsh.sh

# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"


HIST_STAMPS="dd.mm.yyyy"

# Plugins
plugins=(git autojump archlinux)


# User configuration

export HISTCONTROL=ignoreboth

if [[ -n $SSH_CONNECTION ]]; then
	export TERM='xterm-256color'
else
	export TERM='alacritty'
fi


# Custom Aliases
alias zshcfg='nvim $HOME/.config/zsh/.zshrc'
alias srcrc='source $HOME/.config/zsh/.zshrc'
alias sshcfg='nvim ~/.ssh/config'
alias vimcfg='nvim ~/.config/nvim/init.vim'

alias shutdown='shutdown now'
alias restart='reboot'

alias v='nvim'
alias vi='nvim'
alias vim='nvim'

alias pacman='sudo pacman'
alias upgrade='sudo apt-get update && sudo apt-get upgrade -y'
alias update='sudo apt-get update'
alias apt='sudo apt-get'

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;      
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# clear the terminal window
alias cls='clear'

# Directory navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# pacman and paru
alias pacsyu='sudo pacman -Syyu --noconfirm'       # update only standard pkgs
alias parusua="paru -Sua --noconfirm"              # update only AUR pkgs
alias parusyu="paru -Syyu --noconfirm"             # update standard pkgs and AUR pkgs
alias unlock="sudo rm /var/lib/pacman/db.lck"      # remove pacman lock
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'   # remove orphaned packages

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

# confirm before overwriting something
alias cp="cp -iv"
alias mv='mv -iv'
alias rm='rm -iv'

# force remove (Be careful, this can not be undone)
alias fcrm='rm -rfv'

## get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

# shutdown or reboot
alias ssn="shutdown now"
alias sr="reboot"

# Configuration for git dotfiles
alias cfg='/usr/bin/git --git-dir=/home/arctic/.cfg/ --work-tree=/home/arctic'

# Git alias's
alias g='git'
alias gc='git config'
alias gps='git push'
alias gpl='git pull'

# Display Alias's
if [ asgard = "$HOSTNAME" ]; then
	alias rdf='xrandr --output DP-2 --off'
	alias ldf='xrandr --output DP-4 --off'
	alias rdn='xrandr --output DP-2 --auto --right-of DP-4'
	alias ldn='xrandr --output DP-4 --auto --left-of DP-2'
elif [ bifrost = "$HOSTNAME" ]; then
	alias rdf='xrandr --output DP-3 --off'
	alias ldf='xrandr --output eDP-1 --off'
	alias rdn='xrandr --output DP-3 --auto --right-of eDP-1'
	alias ldn='xrandr --output eDP-1 --auto --left-of DP-3'
fi	

# adb Alias's
alias adb='sudo adb'
alias fastboot='sudo fastboot'

# Find Alias's
alias find='sudo find'

# Copy and Paste Alias
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
